﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MaximumCapacity
{
    public class Group
    {
        public Person[] People;

        public Group()
        {
            People = new Person[10];
        }

        public double Intelligence { get { var i = 0.0; foreach (var p in People) i += p.Intelligence; return i / 10; } }
        public double Tolerence { get { var i = 0.0; foreach (var p in People) i += p.Tolerence; return i / 10; } }
        public double Aggression { get { var i = 0.0; foreach (var p in People) i += p.Aggression; return i / 10; } }
        public double Adaptability { get { var i = 0.0; foreach (var p in People) i += p.Adaptability; return i / 10; } }
        public double Healing { get { var i = 0.0; foreach (var p in People) i += p.Healing; return i / 10; } }
        public double Strength { get { var i = 0.0; foreach (var p in People) i += p.Strength; return i / 10; } }
        public double Trade { get { var i = 0.0; foreach (var p in People) i += p.Trade; return i / 10; } }
        public double Engineering { get { var i = 0.0; foreach (var p in People) i += p.Engineering; return i / 10; } }
        public double Tech { get { var i = 0.0; foreach (var p in People) i += p.Tech; return i / 10; } }
    }
}
