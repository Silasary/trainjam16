﻿using UnityEngine;
using System.Collections;

namespace MaximumCapacity
{

    public class PersonPortraitGenerator : MonoBehaviour
    {

        public Sprite[] headSprites;
        public Sprite[] shirtSprites;
        public Sprite[] hairSprites;
        public Sprite[] eyeSprites;
        public Sprite[] noseSprites;
        public Sprite[] mouthSprites;

        public GameObject[] PersonAvatar()
        {
            GameObject[] avatar = new GameObject[6];


            #region Skin Selection
            GameObject head = new GameObject();
            head.AddComponent<SpriteRenderer>();
            head.GetComponent<SpriteRenderer>().sprite = headSprites[Random.Range(0, 4)];
            head.GetComponent<SpriteRenderer>().sortingLayerName = "Character";
            head.GetComponent<SpriteRenderer>().sortingOrder = 0;
            avatar[0] = head;
            #endregion

            #region Shirt Selection
            GameObject shirt = new GameObject();
            shirt.AddComponent<SpriteRenderer>();
            shirt.GetComponent<SpriteRenderer>().sprite = shirtSprites[Random.Range(0, 4)];
            shirt.GetComponent<SpriteRenderer>().sortingLayerName = "Character";
            shirt.GetComponent<SpriteRenderer>().sortingOrder = -1;
            avatar[1] = shirt;
            #endregion

            #region Hair Selection
            GameObject hair = new GameObject();
            hair.AddComponent<SpriteRenderer>();
            hair.GetComponent<SpriteRenderer>().sprite = hairSprites[Random.Range(0, 4)];
            hair.GetComponent<SpriteRenderer>().sortingLayerName = "Character";
            hair.GetComponent<SpriteRenderer>().sortingOrder = 1;
            avatar[2] = hair;
            #endregion

            #region Eye Selection  and Display
            GameObject eye = new GameObject();
            eye.AddComponent<SpriteRenderer>();
            eye.GetComponent<SpriteRenderer>().sprite = eyeSprites[Random.Range(0, 4)];
            eye.GetComponent<SpriteRenderer>().sortingLayerName = "Character";
            eye.GetComponent<SpriteRenderer>().sortingOrder = 1;
            avatar[3] = eye;
            #endregion

            #region Nose Selection
            GameObject nose = new GameObject();
            nose.AddComponent<SpriteRenderer>();
            nose.GetComponent<SpriteRenderer>().sprite = noseSprites[Random.Range(0, 4)];
            nose.GetComponent<SpriteRenderer>().sortingLayerName = "Character";
            nose.GetComponent<SpriteRenderer>().sortingOrder = 1;
            avatar[4] = nose;
            #endregion

            #region Mouth Selection
            GameObject mouth = new GameObject();
            mouth.AddComponent<SpriteRenderer>();
            mouth.GetComponent<SpriteRenderer>().sprite = mouthSprites[Random.Range(0, 4)];
            mouth.GetComponent<SpriteRenderer>().sortingLayerName = "Character";
            mouth.GetComponent<SpriteRenderer>().sortingOrder = 1;
            avatar[5] = mouth;
            #endregion

            return avatar;


        }
        // Use this for initialization
        void Start()
        {


        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
