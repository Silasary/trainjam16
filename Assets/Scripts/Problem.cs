﻿using UnityEngine;

namespace MaximumCapacity
{
    public class Problem : MonoBehaviour
    {
        public ProblemType type;

        public Person DispachedCrewMember;
        private ShipManager Ship;

        void Start()
        {
            Ship = GetComponent<ShipManager>();
        }

        public void Worsen()
        {
            if (DispachedCrewMember != null && DispachedCrewMember.HealthActual > 0)
            {
                var skill = (int)DispachedCrewMember[type.RequiredSkill.ToString()];
                if (skill > type.Difficulty)
                {
                    SendMessage("ShipLog", string.Format("{0} has successfully solved our {1} problem!", DispachedCrewMember.Name, type.name), SendMessageOptions.DontRequireReceiver);
                    Destroy(this);
                    return;
                }
                else if (Random.Range(skill, 100) > type.Difficulty)
                {
                    SendMessage("ShipLog", string.Format("{0} has solved our {1} problem, with only a little trouble", DispachedCrewMember.Name, type.name), SendMessageOptions.DontRequireReceiver);
                    DispachedCrewMember.HealthActual -= Random.Range(1, type.Difficulty - skill);
                    Destroy(this);
                    return;
                }
                else
                {
                    if (Random.Range(0, 100) < type.Danger)
                    {
                        SendMessage("ShipLog", string.Format("{0} has injured themself trying to solve our {1} problem!", DispachedCrewMember.Name, type.name), SendMessageOptions.DontRequireReceiver);
                        DispachedCrewMember.HealthActual -= 25;
                    }
                }
            }
            else if (Random.Range(0, 2) == 1)
            {
                DispachedCrewMember = Ship.Crew[Random.Range(0, Ship.Crew.Length - 1)];
                if (DispachedCrewMember.HealthActual > 0)
                    SendMessage("ShipLog", string.Format("{0} has taken it on themself to deal with our {1} problem", DispachedCrewMember.Name, type.name), SendMessageOptions.DontRequireReceiver);

            }

            switch (type.Affects)
            {
                case ShipSystem.Resources:
                    Ship.Resources -= type.Impact;
                    break;
                case ShipSystem.Condition:
                    Ship.Condition -= type.Impact;
                    break;
                case ShipSystem.Morale:
                    break;
                case ShipSystem.Health:
                    break;
                default:
                    break;
            }

        }

     
    }
}