﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace MaximumCapacity
{
    //[AddComponentMenu("MaximumCapacity/Person")]
    [DisallowMultipleComponent]
    [Serializable]
    public class Person : IndexableClass<double>
    {
        /// <summary>
        /// Affects Proficiency?
        /// Also, Old Age, Too Young, etc.
        /// </summary>
        public double Age;
        
        public double Intelligence;
        public double Tolerence;
        public double Aggression;
        public double Adaptability;
        public double Healing;
        public double Strength;
        public double Trade;
        public double Engineering;
        public double Tech;

        public bool CanReproduce;
        public bool HasUterus;

        public double HealthActual;
        public double HealthVisible;

        public string Name;

        public double Morale;

        public Occupations Occupation;


    }
}
