﻿//using System;
using Assets.Librarys;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

//using Range = System.Tuple<int, int>;
using OccupationData = System.Collections.Generic.Dictionary<MaximumCapacity.Skills, Assets.Librarys.Range>;

namespace MaximumCapacity
{
    public enum Skills
    {
        Age, Intelligence, Tolerence, Aggression, Adaptability, Healing, Strength, Trade, Engineering, Tech
    };

    public enum Occupations
    {
        Labor, Agriculture, Technical, Management, Education, Child, Medical,
    };


    /// <summary>
    /// Generates people!
    /// </summary>
    public class PersonGenerator : MonoBehaviour
    {

        static Dictionary<Occupations, OccupationData> SkillData = new Dictionary<Occupations, OccupationData>()
        {
            {
                Occupations.Medical,
                new OccupationData() {
                    { Skills.Intelligence,  new Range(70,90) },
                    { Skills.Tolerence, new Range(55,105) },
                    { Skills.Aggression, new Range(15,35) },
                    { Skills.Adaptability, new Range(40,80) },
                    { Skills.Age, new Range(25,60) },
                    { Skills.Healing, new Range(88,92) },
                    { Skills.Strength, new Range(20,80) },
                    { Skills.Trade, new Range(0,30) },
                    { Skills.Engineering, new Range(20,60) },
                    { Skills.Tech, new Range(60,80) },
                }
            },
            {
                Occupations.Agriculture,
                new OccupationData()
                {
                    { Skills.Intelligence,  new Range(70,90) },
                    { Skills.Tolerence, new Range(30,70) },
                    { Skills.Aggression, new Range(35,80) },
                    { Skills.Adaptability, new Range(70,90) },
                    { Skills.Age, new Range(20,80) },
                    { Skills.Healing, new Range(60,80) },
                    { Skills.Strength, new Range(80,90) },
                    { Skills.Trade, new Range(80,95) },
                    { Skills.Engineering, new Range(70,90) },
                    { Skills.Tech, new Range(60,80) },
                    // Filled In ~Robby
                }
            },
            {
                Occupations.Labor,
                new OccupationData()
                {
                    { Skills.Intelligence,  new Range(40,70) },
                    { Skills.Tolerence, new Range(20,80) },
                    { Skills.Aggression, new Range(20,100) },
                    { Skills.Adaptability, new Range(15,65) },
                    { Skills.Age, new Range(20,80) },
                    { Skills.Healing, new Range(20,40) },
                    { Skills.Strength, new Range(80,90) },
                    { Skills.Trade, new Range(70,90) },
                    { Skills.Engineering, new Range(10,40) },
                    { Skills.Tech, new Range(0,50) },
					// Filled In ~Robby
				}
            },
            {
                Occupations.Technical,
                new OccupationData()
                {
                    { Skills.Intelligence,  new Range(75,85) },
                    { Skills.Tolerence, new Range(20,80) },
                    { Skills.Aggression, new Range(20,50) },
                    { Skills.Adaptability, new Range(50,90) },
                    { Skills.Age, new Range(20,50) },
                    { Skills.Healing, new Range(20,40) },
                    { Skills.Strength, new Range(55,75) },
                    { Skills.Trade, new Range(40,80) },
                    { Skills.Engineering, new Range(65,95) },
                    { Skills.Tech, new Range(90,100) },
					// Filled In ~Robby
				}
            },
            {
                Occupations.Child,
                new OccupationData()
                {
                    { Skills.Intelligence,  new Range(60,80) },
                    { Skills.Tolerence, new Range(75,100) },
                    { Skills.Aggression, new Range(0,20) },
                    { Skills.Adaptability, new Range(75,95) },
                    { Skills.Age, new Range(1,16) },
                    { Skills.Healing, new Range(0,10) },
                    { Skills.Strength, new Range(5,20) },
                    { Skills.Trade, new Range(5,10) }, //(Function of Age) },
					{ Skills.Engineering, new Range(0,15) }, //Function of Age },
					{ Skills.Tech, new Range(90,100) },
					// Filled In ~Robby

				}
            },

                {
                    Occupations.Education,
                    new OccupationData()
                    {
                        { Skills.Intelligence,  new Range(50,100) },
                        { Skills.Tolerence, new Range(75,95) },
                        { Skills.Aggression, new Range(20,50) },
                        { Skills.Adaptability, new Range(30,70) },
                        { Skills.Age, new Range(25,60) },
                        { Skills.Healing, new Range(30,50) },
                        { Skills.Strength, new Range(20,80) },
                        { Skills.Trade, new Range(10,40) },
                        { Skills.Engineering, new Range(30,70) },
                        { Skills.Tech, new Range(30,70) },
						// Filled In ~Robby
					}

                },
                {
                    Occupations.Management,
                    new OccupationData()
                    {
                        { Skills.Intelligence,  new Range(25,75) },
                        { Skills.Tolerence, new Range(25,75) },
                        { Skills.Aggression, new Range(20,60) },
                        { Skills.Adaptability, new Range(40,60) },
                        { Skills.Age, new Range(25,60) },
                        { Skills.Healing, new Range(0,10) },
                        { Skills.Strength, new Range(10,50) },
                        { Skills.Trade, new Range(10,30) },
                        { Skills.Engineering, new Range(10,50) },
                        { Skills.Tech, new Range(30,70) },
						// Filled In ~Robby
					}
                }
        };


        public static Group CreateGroup()
        {
            var group = new Group();
            group.People = new Person[10];
            for (int j = 0; j < group.People.Length; j++)
            {
                group.People[j] = GeneratePerson();
            }
            return group;
        }

        Group group = new Group();
        
        void Start()
        {

            group = CreateGroup();

            var textBox = GameObject.FindWithTag("groupTextTag");
            for (int x = 0; x < 2; x++)
            {
                #region Output Person Stats
                textBox.GetComponent<Text>().text = textBox.GetComponent<Text>().text + 
                    //"Name: " + group.People[x].Name +
                    "\nAge: " + group.People[x].Age.ToString() +
                    "\nIntelligence: " + group.People[x].Intelligence.ToString() +
                    "\nTolerance: " + group.People[x].Tolerence.ToString() +
                    "\nAggression: " + group.People[x].Aggression.ToString() +
                    "\nAdaptability: " + group.People[x].Adaptability.ToString() +
                    "\nHealing: " + group.People[x].Healing.ToString() +
                    "\nStrength: " + group.People[x].Strength.ToString() +
                    "\nTrade: " + group.People[x].Trade.ToString() +
                    "\nEngineering: " + group.People[x].Engineering.ToString() +
                    "\nTech: " + group.People[x].Tech.ToString() +
                    //"\nCan Reproduce: " + group.People[x].CanReproduce.ToString() +
                    //"\nHas Uterus: " + group.People[x].HasUterus.ToString() +
                    "\nHealth: " + group.People[x].HealthVisible.ToString() +
                    "\nMorale: " + group.People[x].Morale.ToString() +
                    "\nOccupation: " + group.People[x].Occupation + "\n\n\n";
                #endregion
            }

            var textBox2 = GameObject.FindWithTag("groupTextTag2");
            for (int x = 2; x < 4; x++)
            {
                #region Output Person Stats
                textBox2.GetComponent<Text>().text = textBox2.GetComponent<Text>().text + 
                    //"Name: " + group.People[x].Name +
                    "\nAge: " + group.People[x].Age.ToString() +
                    "\nIntelligence: " + group.People[x].Intelligence.ToString() +
                    "\nTolerance: " + group.People[x].Tolerence.ToString() +
                    "\nAggression: " + group.People[x].Aggression.ToString() +
                    "\nAdaptability: " + group.People[x].Adaptability.ToString() +
                    "\nHealing: " + group.People[x].Healing.ToString() +
                    "\nStrength: " + group.People[x].Strength.ToString() +
                    "\nTrade: " + group.People[x].Trade.ToString() +
                    "\nEngineering: " + group.People[x].Engineering.ToString() +
                    "\nTech: " + group.People[x].Tech.ToString() +
                    //"\nCan Reproduce: " + group.People[x].CanReproduce.ToString() +
                    //"\nHas Uterus: " + group.People[x].HasUterus.ToString() +
                    "\nHealth: " + group.People[x].HealthVisible.ToString() +
                    "\nMorale: " + group.People[x].Morale.ToString() +
                    "\nOccupation: " + group.People[x].Occupation + "\n\n\n";
                #endregion
            }

            var textBox3 = GameObject.FindWithTag("groupTextTag3");
            for (int x = 4; x < 6; x++)
            {
                #region Output Person Stats
                textBox3.GetComponent<Text>().text = textBox3.GetComponent<Text>().text + 
                    //"Name: " + group.People[x].Name +
                    "\nAge: " + group.People[x].Age.ToString() +
                    "\nIntelligence: " + group.People[x].Intelligence.ToString() +
                    "\nTolerance: " + group.People[x].Tolerence.ToString() +
                    "\nAggression: " + group.People[x].Aggression.ToString() +
                    "\nAdaptability: " + group.People[x].Adaptability.ToString() +
                    "\nHealing: " + group.People[x].Healing.ToString() +
                    "\nStrength: " + group.People[x].Strength.ToString() +
                    "\nTrade: " + group.People[x].Trade.ToString() +
                    "\nEngineering: " + group.People[x].Engineering.ToString() +
                    "\nTech: " + group.People[x].Tech.ToString() +
                    //"\nCan Reproduce: " + group.People[x].CanReproduce.ToString() +
                    //"\nHas Uterus: " + group.People[x].HasUterus.ToString() +
                    "\nHealth: " + group.People[x].HealthVisible.ToString() +
                    "\nMorale: " + group.People[x].Morale.ToString() +
                    "\nOccupation: " + group.People[x].Occupation + "\n\n\n";
                #endregion
            }

            var textBox4 = GameObject.FindWithTag("groupTextTag4");
            for (int x = 6; x < 8; x++)
            {
                #region Output Person Stats
                textBox4.GetComponent<Text>().text = textBox4.GetComponent<Text>().text + 
                    //"Name: " + group.People[x].Name +
                    "\nAge: " + group.People[x].Age.ToString() +
                    "\nIntelligence: " + group.People[x].Intelligence.ToString() +
                    "\nTolerance: " + group.People[x].Tolerence.ToString() +
                    "\nAggression: " + group.People[x].Aggression.ToString() +
                    "\nAdaptability: " + group.People[x].Adaptability.ToString() +
                    "\nHealing: " + group.People[x].Healing.ToString() +
                    "\nStrength: " + group.People[x].Strength.ToString() +
                    "\nTrade: " + group.People[x].Trade.ToString() +
                    "\nEngineering: " + group.People[x].Engineering.ToString() +
                    "\nTech: " + group.People[x].Tech.ToString() +
                    //"\nCan Reproduce: " + group.People[x].CanReproduce.ToString() +
                    //"\nHas Uterus: " + group.People[x].HasUterus.ToString() +
                    "\nHealth: " + group.People[x].HealthVisible.ToString() +
                    "\nMorale: " + group.People[x].Morale.ToString() +
                    "\nOccupation: " + group.People[x].Occupation + "\n\n\n";
                #endregion
            }

            var textBox5 = GameObject.FindWithTag("groupTextTag5");
            for (int x = 8; x < 10; x++)
            {
                #region Output Person Stats
                textBox5.GetComponent<Text>().text = textBox5.GetComponent<Text>().text + 
                    //"Name: " + group.People[x].Name +
                    "\nAge: " + group.People[x].Age.ToString() +
                    "\nIntelligence: " + group.People[x].Intelligence.ToString() +
                    "\nTolerance: " + group.People[x].Tolerence.ToString() +
                    "\nAggression: " + group.People[x].Aggression.ToString() +
                    "\nAdaptability: " + group.People[x].Adaptability.ToString() +
                    "\nHealing: " + group.People[x].Healing.ToString() +
                    "\nStrength: " + group.People[x].Strength.ToString() +
                    "\nTrade: " + group.People[x].Trade.ToString() +
                    "\nEngineering: " + group.People[x].Engineering.ToString() +
                    "\nTech: " + group.People[x].Tech.ToString() +
                    //"\nCan Reproduce: " + group.People[x].CanReproduce.ToString() +
                    //"\nHas Uterus: " + group.People[x].HasUterus.ToString() +
                    "\nHealth: " + group.People[x].HealthVisible.ToString() +
                    "\nMorale: " + group.People[x].Morale.ToString() +
                    "\nOccupation: " + group.People[x].Occupation + "\n\n\n";
                #endregion
            }
        }

        

        void AddToCrew()//Complete
        {
            

            #region If Crew has 40 Members
            if (GameObject.FindWithTag("ShipController").GetComponent<ShipManager>().Crew[39].Age != 0 && GameObject.FindWithTag("ShipController").GetComponent<ShipManager>().Crew[49].Age == 0)
            {
                for (int z = 0; z < 10; z++)
                {
                    GameObject.FindWithTag("ShipController").GetComponent<ShipManager>().Crew[40 + z] = group.People[z];
                }
            }
            #endregion

            #region If Crew has 30 Members
            if (GameObject.FindWithTag("ShipController").GetComponent<ShipManager>().Crew[29].Age != 0 && GameObject.FindWithTag("ShipController").GetComponent<ShipManager>().Crew[39].Age == 0)
            {
                for (int z = 0; z < 10; z++)
                {
                    GameObject.FindWithTag("ShipController").GetComponent<ShipManager>().Crew[30 + z] = group.People[z];
                }
            }
            #endregion

            #region If Crew has 20 Members
            if (GameObject.FindWithTag("ShipController").GetComponent<ShipManager>().Crew[19].Age != 0 && GameObject.FindWithTag("ShipController").GetComponent<ShipManager>().Crew[29].Age == 0)
            {
                for (int z = 0; z < 10; z++)
                {
                    GameObject.FindWithTag("ShipController").GetComponent<ShipManager>().Crew[20 + z] = group.People[z];
                }
            }
            #endregion

            #region If Crew has 10 Memebrs
            if (GameObject.FindWithTag("ShipController").GetComponent<ShipManager>().Crew[9].Age != 0 && GameObject.FindWithTag("ShipController").GetComponent<ShipManager>().Crew[19].Age == 0)
            {
                for (int z = 0; z < 10; z++)
                {
                    GameObject.FindWithTag("ShipController").GetComponent<ShipManager>().Crew[10 + z] = group.People[z];
                }
            }
            #endregion


            #region If Crew has 0 Members
            if (GameObject.FindWithTag("ShipController").GetComponent<ShipManager>().Crew[9].Age == 0)
            {
                for (int z = 0; z < 10; z++)
                {
                    GameObject.FindWithTag("ShipController").GetComponent<ShipManager>().Crew[z] = group.People[z];
                }
            }


            #endregion

            



            GameObject.FindWithTag("groupTextTag").GetComponent<Text>().text = "";
            GameObject.FindWithTag("groupTextTag2").GetComponent<Text>().text = "";
            GameObject.FindWithTag("groupTextTag3").GetComponent<Text>().text = "";
            GameObject.FindWithTag("groupTextTag4").GetComponent<Text>().text = "";
            GameObject.FindWithTag("groupTextTag5").GetComponent<Text>().text = "";
            if (GameObject.FindWithTag("ShipController").GetComponent<ShipManager>().Crew[49].Age == 0)
            {
                group = CreateGroup();

                var textBox = GameObject.FindWithTag("groupTextTag");
                for (int x = 0; x < 2; x++)
                {
                    #region Output Person Stats
                    textBox.GetComponent<Text>().text = textBox.GetComponent<Text>().text + 
                        //"Name: " + group.People[x].Name +
                        "\nAge: " + group.People[x].Age.ToString() +
                        "\nIntelligence: " + group.People[x].Intelligence.ToString() +
                        "\nTolerance: " + group.People[x].Tolerence.ToString() +
                        "\nAggression: " + group.People[x].Aggression.ToString() +
                        "\nAdaptability: " + group.People[x].Adaptability.ToString() +
                        "\nHealing: " + group.People[x].Healing.ToString() +
                        "\nStrength: " + group.People[x].Strength.ToString() +
                        "\nTrade: " + group.People[x].Trade.ToString() +
                        "\nEngineering: " + group.People[x].Engineering.ToString() +
                        "\nTech: " + group.People[x].Tech.ToString() +
                        //"\nCan Reproduce: " + group.People[x].CanReproduce.ToString() +
                        //"\nHas Uterus: " + group.People[x].HasUterus.ToString() +
                        "\nHealth: " + group.People[x].HealthVisible.ToString() +
                        "\nMorale: " + group.People[x].Morale.ToString() +
                        "\nOccupation: " + group.People[x].Occupation + "\n\n\n";
                    #endregion
                }

                var textBox2 = GameObject.FindWithTag("groupTextTag2");
                for (int x = 2; x < 4; x++)
                {
                    #region Output Person Stats
                    textBox2.GetComponent<Text>().text = textBox2.GetComponent<Text>().text + 
                        //"Name: " + group.People[x].Name +
                        "\nAge: " + group.People[x].Age.ToString() +
                        "\nIntelligence: " + group.People[x].Intelligence.ToString() +
                        "\nTolerance: " + group.People[x].Tolerence.ToString() +
                        "\nAggression: " + group.People[x].Aggression.ToString() +
                        "\nAdaptability: " + group.People[x].Adaptability.ToString() +
                        "\nHealing: " + group.People[x].Healing.ToString() +
                        "\nStrength: " + group.People[x].Strength.ToString() +
                        "\nTrade: " + group.People[x].Trade.ToString() +
                        "\nEngineering: " + group.People[x].Engineering.ToString() +
                        "\nTech: " + group.People[x].Tech.ToString() +
                        //"\nCan Reproduce: " + group.People[x].CanReproduce.ToString() +
                        //"\nHas Uterus: " + group.People[x].HasUterus.ToString() +
                        "\nHealth: " + group.People[x].HealthVisible.ToString() +
                        "\nMorale: " + group.People[x].Morale.ToString() +
                        "\nOccupation: " + group.People[x].Occupation + "\n\n\n";
                    #endregion
                }

                var textBox3 = GameObject.FindWithTag("groupTextTag3");
                for (int x = 4; x < 6; x++)
                {
                    #region Output Person Stats
                    textBox3.GetComponent<Text>().text = textBox3.GetComponent<Text>().text + 
                        //"Name: " + group.People[x].Name +
                        "\nAge: " + group.People[x].Age.ToString() +
                        "\nIntelligence: " + group.People[x].Intelligence.ToString() +
                        "\nTolerance: " + group.People[x].Tolerence.ToString() +
                        "\nAggression: " + group.People[x].Aggression.ToString() +
                        "\nAdaptability: " + group.People[x].Adaptability.ToString() +
                        "\nHealing: " + group.People[x].Healing.ToString() +
                        "\nStrength: " + group.People[x].Strength.ToString() +
                        "\nTrade: " + group.People[x].Trade.ToString() +
                        "\nEngineering: " + group.People[x].Engineering.ToString() +
                        "\nTech: " + group.People[x].Tech.ToString() +
                        //"\nCan Reproduce: " + group.People[x].CanReproduce.ToString() +
                        //"\nHas Uterus: " + group.People[x].HasUterus.ToString() +
                        "\nHealth: " + group.People[x].HealthVisible.ToString() +
                        "\nMorale: " + group.People[x].Morale.ToString() +
                        "\nOccupation: " + group.People[x].Occupation + "\n\n\n";
                    #endregion
                }

                var textBox4 = GameObject.FindWithTag("groupTextTag4");
                for (int x = 6; x < 8; x++)
                {
                    #region Output Person Stats
                    textBox4.GetComponent<Text>().text = textBox4.GetComponent<Text>().text + 
                        //"Name: " + group.People[x].Name +
                        "\nAge: " + group.People[x].Age.ToString() +
                        "\nIntelligence: " + group.People[x].Intelligence.ToString() +
                        "\nTolerance: " + group.People[x].Tolerence.ToString() +
                        "\nAggression: " + group.People[x].Aggression.ToString() +
                        "\nAdaptability: " + group.People[x].Adaptability.ToString() +
                        "\nHealing: " + group.People[x].Healing.ToString() +
                        "\nStrength: " + group.People[x].Strength.ToString() +
                        "\nTrade: " + group.People[x].Trade.ToString() +
                        "\nEngineering: " + group.People[x].Engineering.ToString() +
                        "\nTech: " + group.People[x].Tech.ToString() +
                        //"\nCan Reproduce: " + group.People[x].CanReproduce.ToString() +
                        //"\nHas Uterus: " + group.People[x].HasUterus.ToString() +
                        "\nHealth: " + group.People[x].HealthVisible.ToString() +
                        "\nMorale: " + group.People[x].Morale.ToString() +
                        "\nOccupation: " + group.People[x].Occupation + "\n\n\n";
                    #endregion
                }

                var textBox5 = GameObject.FindWithTag("groupTextTag5");
                for (int x = 8; x < 10; x++)
                {
                    #region Output Person Stats
                    textBox5.GetComponent<Text>().text = textBox5.GetComponent<Text>().text + 
                        //"Name: " + group.People[x].Name +
                        "\nAge: " + group.People[x].Age.ToString() +
                        "\nIntelligence: " + group.People[x].Intelligence.ToString() +
                        "\nTolerance: " + group.People[x].Tolerence.ToString() +
                        "\nAggression: " + group.People[x].Aggression.ToString() +
                        "\nAdaptability: " + group.People[x].Adaptability.ToString() +
                        "\nHealing: " + group.People[x].Healing.ToString() +
                        "\nStrength: " + group.People[x].Strength.ToString() +
                        "\nTrade: " + group.People[x].Trade.ToString() +
                        "\nEngineering: " + group.People[x].Engineering.ToString() +
                        "\nTech: " + group.People[x].Tech.ToString() +
                        //"\nCan Reproduce: " + group.People[x].CanReproduce.ToString() +
                        //"\nHas Uterus: " + group.People[x].HasUterus.ToString() +
                        "\nHealth: " + group.People[x].HealthVisible.ToString() +
                        "\nMorale: " + group.People[x].Morale.ToString() +
                        "\nOccupation: " + group.People[x].Occupation + "\n\n\n";
                    #endregion
                }
            }
            #region If Crew is Full
            if (GameObject.FindWithTag("ShipController").GetComponent<ShipManager>().Crew[49].Age != 0)
            {
                GameObject.FindWithTag("launchTag").GetComponent<Text>().text = "LAUNCH";

            }
            #endregion

        }

        public void Decline()
        {
            GameObject.FindWithTag("groupTextTag").GetComponent<Text>().text = "";
            GameObject.FindWithTag("groupTextTag2").GetComponent<Text>().text = "";
            GameObject.FindWithTag("groupTextTag3").GetComponent<Text>().text = "";
            GameObject.FindWithTag("groupTextTag4").GetComponent<Text>().text = "";
            GameObject.FindWithTag("groupTextTag5").GetComponent<Text>().text = "";
            if (GameObject.FindWithTag("ShipController").GetComponent<ShipManager>().Crew[49].Age == 0)
            {
                group = CreateGroup();

                var textBox = GameObject.FindWithTag("groupTextTag");
                for (int x = 0; x < 2; x++)
                {
                    #region Output Person Stats
                    textBox.GetComponent<Text>().text = textBox.GetComponent<Text>().text + "Name: " + group.People[x].Name +
                        "\nAge: " + group.People[x].Age.ToString() +
                        "\nIntelligence: " + group.People[x].Intelligence.ToString() +
                        "\nTolerance: " + group.People[x].Tolerence.ToString() +
                        "\nAggression: " + group.People[x].Aggression.ToString() +
                        "\nAdaptability: " + group.People[x].Adaptability.ToString() +
                        "\nHealing: " + group.People[x].Healing.ToString() +
                        "\nStrength: " + group.People[x].Strength.ToString() +
                        "\nTrade: " + group.People[x].Trade.ToString() +
                        "\nEngineering: " + group.People[x].Engineering.ToString() +
                        "\nTech: " + group.People[x].Tech.ToString() +
                        "\nCan Reproduce: " + group.People[x].CanReproduce.ToString() +
                        "\nHas Uterus: " + group.People[x].HasUterus.ToString() +
                        "\nHealth: " + group.People[x].HealthVisible.ToString() +
                        "\nMorale: " + group.People[x].Morale.ToString() +
                        "\nOccupation: " + group.People[x].Occupation + "\n\n\n";
                    #endregion
                }

                var textBox2 = GameObject.FindWithTag("groupTextTag2");
                for (int x = 2; x < 4; x++)
                {
                    #region Output Person Stats
                    textBox2.GetComponent<Text>().text = textBox2.GetComponent<Text>().text + "Name: " + group.People[x].Name +
                        "\nAge: " + group.People[x].Age.ToString() +
                        "\nIntelligence: " + group.People[x].Intelligence.ToString() +
                        "\nTolerance: " + group.People[x].Tolerence.ToString() +
                        "\nAggression: " + group.People[x].Aggression.ToString() +
                        "\nAdaptability: " + group.People[x].Adaptability.ToString() +
                        "\nHealing: " + group.People[x].Healing.ToString() +
                        "\nStrength: " + group.People[x].Strength.ToString() +
                        "\nTrade: " + group.People[x].Trade.ToString() +
                        "\nEngineering: " + group.People[x].Engineering.ToString() +
                        "\nTech: " + group.People[x].Tech.ToString() +
                        "\nCan Reproduce: " + group.People[x].CanReproduce.ToString() +
                        "\nHas Uterus: " + group.People[x].HasUterus.ToString() +
                        "\nHealth: " + group.People[x].HealthVisible.ToString() +
                        "\nMorale: " + group.People[x].Morale.ToString() +
                        "\nOccupation: " + group.People[x].Occupation + "\n\n\n";
                    #endregion
                }

                var textBox3 = GameObject.FindWithTag("groupTextTag3");
                for (int x = 4; x < 6; x++)
                {
                    #region Output Person Stats
                    textBox3.GetComponent<Text>().text = textBox3.GetComponent<Text>().text + "Name: " + group.People[x].Name +
                        "\nAge: " + group.People[x].Age.ToString() +
                        "\nIntelligence: " + group.People[x].Intelligence.ToString() +
                        "\nTolerance: " + group.People[x].Tolerence.ToString() +
                        "\nAggression: " + group.People[x].Aggression.ToString() +
                        "\nAdaptability: " + group.People[x].Adaptability.ToString() +
                        "\nHealing: " + group.People[x].Healing.ToString() +
                        "\nStrength: " + group.People[x].Strength.ToString() +
                        "\nTrade: " + group.People[x].Trade.ToString() +
                        "\nEngineering: " + group.People[x].Engineering.ToString() +
                        "\nTech: " + group.People[x].Tech.ToString() +
                        "\nCan Reproduce: " + group.People[x].CanReproduce.ToString() +
                        "\nHas Uterus: " + group.People[x].HasUterus.ToString() +
                        "\nHealth: " + group.People[x].HealthVisible.ToString() +
                        "\nMorale: " + group.People[x].Morale.ToString() +
                        "\nOccupation: " + group.People[x].Occupation + "\n\n\n";
                    #endregion
                }

                var textBox4 = GameObject.FindWithTag("groupTextTag4");
                for (int x = 6; x < 8; x++)
                {
                    #region Output Person Stats
                    textBox4.GetComponent<Text>().text = textBox4.GetComponent<Text>().text + "Name: " + group.People[x].Name +
                        "\nAge: " + group.People[x].Age.ToString() +
                        "\nIntelligence: " + group.People[x].Intelligence.ToString() +
                        "\nTolerance: " + group.People[x].Tolerence.ToString() +
                        "\nAggression: " + group.People[x].Aggression.ToString() +
                        "\nAdaptability: " + group.People[x].Adaptability.ToString() +
                        "\nHealing: " + group.People[x].Healing.ToString() +
                        "\nStrength: " + group.People[x].Strength.ToString() +
                        "\nTrade: " + group.People[x].Trade.ToString() +
                        "\nEngineering: " + group.People[x].Engineering.ToString() +
                        "\nTech: " + group.People[x].Tech.ToString() +
                        "\nCan Reproduce: " + group.People[x].CanReproduce.ToString() +
                        "\nHas Uterus: " + group.People[x].HasUterus.ToString() +
                        "\nHealth: " + group.People[x].HealthVisible.ToString() +
                        "\nMorale: " + group.People[x].Morale.ToString() +
                        "\nOccupation: " + group.People[x].Occupation + "\n\n\n";
                    #endregion
                }

                var textBox5 = GameObject.FindWithTag("groupTextTag5");
                for (int x = 8; x < 10; x++)
                {
                    #region Output Person Stats
                    textBox5.GetComponent<Text>().text = textBox5.GetComponent<Text>().text + "Name: " + group.People[x].Name +
                        "\nAge: " + group.People[x].Age.ToString() +
                        "\nIntelligence: " + group.People[x].Intelligence.ToString() +
                        "\nTolerance: " + group.People[x].Tolerence.ToString() +
                        "\nAggression: " + group.People[x].Aggression.ToString() +
                        "\nAdaptability: " + group.People[x].Adaptability.ToString() +
                        "\nHealing: " + group.People[x].Healing.ToString() +
                        "\nStrength: " + group.People[x].Strength.ToString() +
                        "\nTrade: " + group.People[x].Trade.ToString() +
                        "\nEngineering: " + group.People[x].Engineering.ToString() +
                        "\nTech: " + group.People[x].Tech.ToString() +
                        "\nCan Reproduce: " + group.People[x].CanReproduce.ToString() +
                        "\nHas Uterus: " + group.People[x].HasUterus.ToString() +
                        "\nHealth: " + group.People[x].HealthVisible.ToString() +
                        "\nMorale: " + group.People[x].Morale.ToString() +
                        "\nOccupation: " + group.People[x].Occupation + "\n\n\n";
                    #endregion
                }
            }
            
        }

        public void Launch()
        {
            GameObject.FindWithTag("groupPanel").SetActive(false);
            GameObject.FindWithTag("shipPanelTag").GetComponent<CanvasGroup>().alpha = 1;
            GameObject.FindWithTag("shipPanelTag").GetComponent<CanvasGroup>().interactable = true;



        }

        private static Occupations GenerateOccupation()
        {
            //skew the probability of specific professions:
            //Medical: 5%
            //Technical: 15%
            //Agricultural: 15%
            //Education: 15%
            //Management: 15%
            //Child: 10%
            //Labor: 25%

            int value;
            int value1 = Random.Range(0, 100);
            if (value1 < 35)
            {
                value = value1 % 7;
            }
            else if (value1 < 65)
            {
                value = (value1 - 35) % 6;
            }
            else if (value1 < 90)
            {
                value = (value1 - 65) % 5;
            }
            else {
                value = 0;
            }
            return (Occupations)value;
        }

        private static Person GeneratePerson()
        {
            var person = new Person();
            var occupation = GenerateOccupation();
            person.Occupation = occupation;

            person.Adaptability = SkillData[occupation][Skills.Adaptability].GetRandomPoint();
            person.Age = SkillData[occupation][Skills.Age].GetRandomPoint();
            person.Aggression = SkillData[occupation][Skills.Aggression].GetRandomPoint();
            //person.CanReproduce = 
            person.Engineering = SkillData[occupation][Skills.Engineering].GetRandomPoint();
            person.HasUterus = Random.Range(0, 1) == 1;
            person.Healing = SkillData[occupation][Skills.Healing].GetRandomPoint();
            person.Intelligence = SkillData[occupation][Skills.Intelligence].GetRandomPoint();
            person.Strength = SkillData[occupation][Skills.Strength].GetRandomPoint();
            person.Tech = SkillData[occupation][Skills.Tech].GetRandomPoint();
            person.Tolerence = SkillData[occupation][Skills.Tolerence].GetRandomPoint();
            person.Trade = SkillData[occupation][Skills.Trade].GetRandomPoint();

            person.HealthActual = Random.Range(20, 80);
            person.HealthVisible = person.HealthActual + Random.Range(-5, 20);

            person.Morale = Random.Range(80,100);



            return person;
        }
    }
}
