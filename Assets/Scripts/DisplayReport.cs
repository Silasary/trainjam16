﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;



namespace MaximumCapacity
{
    public class DisplayReport : MonoBehaviour
    {

        public void ShowReport()
        {
            var textBox = GameObject.FindWithTag("reportTag");
            var report = GameObject.FindWithTag("ShipController");


                #region Output Person Stats
                textBox.GetComponent<Text>().text = 
                    "Resources: " + report.GetComponent<ShipManager>().Resources.ToString() +
                    "\nCondition: " + report.GetComponent<ShipManager>().Condition.ToString() +
                    "\nTotal Morale: " + report.GetComponent<ShipManager>().TotalMorale.ToString() +
                    "\nLiving Crew: " + report.GetComponent<ShipManager>().LivingCrew.ToString() +
                    "\n\n\n";
                #endregion
            
        }
    }
}