﻿using UnityEngine;

namespace MaximumCapacity
{
    [CreateAssetMenu(fileName = "NewProblem", menuName = "Ship Problem")]
    public class ProblemType : ScriptableObject
    {
        public Skills RequiredSkill;
        [Range(0,100)]
        public int Difficulty;
        [Range(0, 100)]
        public int Danger;

        [Space]
        public ShipSystem Affects;
        [Range(0,20)]
        public int Impact;
        
        [Space]
        [TextArea]
        public string Description;

    }
}