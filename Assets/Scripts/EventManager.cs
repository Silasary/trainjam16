﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MaximumCapacity
{

    public class EventManager : MonoBehaviour
    {

        ShipManager Ship;
        List<Problem> Problems = new List<Problem>();

        public ProblemType[] ProblemTypes;
        // Use this for initialization
        void Start()
        {
            Ship = GetComponent<ShipManager>();
        }

        [Space]
        public bool Step;

        // Update is called once per frame
        void Update()
        {
            if (Step)
            {
                Step = false;
                MakeReport();
            }
        }

        public void MakeReport()
        {
            Ship.Refresh();
            GetComponents<Problem>(Problems);
            if (Problems.Any())
            {
                SendMessage("Worsen");
            }
            else if (Random.Range(1,5) == 1)
            {
                NewProblem();
            }
            double Morale = 0;
            double Education = 0;
            foreach (var person in Ship.Crew)
            {
                if (person.HealthActual > 0)
                {
                    Ship.Resources--;
                    switch (person.Occupation)
                    {
                        case Occupations.Labor:
                        case Occupations.Agriculture:
                            Ship.Resources += (person.Strength + person.Trade) / 50 * (person.Morale / 100);
                            break;
                        case Occupations.Technical:
                            Ship.Condition += person.Engineering / 30 * (person.Morale / 100);
                            break;
                        case Occupations.Management:
                            Morale += (person.Tolerence - 50) / 5 * (person.Morale / 100);     
                            break;
                        case Occupations.Education:
                            Education += 3;
                            break;
                        case Occupations.Child:
                            Education--;
                            if (person.Morale < 30)
                                Morale--;
                            break;
                        case Occupations.Medical:

                            break;
                        default:
                            break;
                    }
                }
            }
            foreach (var person in Ship.Crew)
            {
                person.Morale += Morale;
                if (person.Occupation == Occupations.Child)
                {
                    person.Morale += Education;
                }
                if (person.Morale > 100)
                    person.Morale = 100;
            }
            if (Ship.Resources > 200)
                Ship.Resources = 200;
            if (Ship.Condition > 100)
                Ship.Condition = 100;
            SendMessage("ShowReport");
        }

        

        void ShipLog(string log)
        {
            Debug.Log(log);
        }

        private void NewProblem()
        {

            var prob = gameObject.AddComponent<Problem>();
            prob.type = ProblemTypes[Random.Range(0, ProblemTypes.Length - 1)];
            SendMessage("ShipLog", string.Format("Alert: {0}!", prob.type.name));
            Problems.Add(prob);
        }
    }
}