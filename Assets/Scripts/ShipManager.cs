﻿using UnityEngine;
using System.Collections;
using System.Linq;

namespace MaximumCapacity
{
    public enum ShipSystem { Resources, Condition, Morale, Health };
    public class ShipManager : MonoBehaviour
    {

        public Person[] Crew;

        [Range(0,200)]
        public double Resources;
        [Range(0,100)]
        public double Condition;

        [Space]
        public double TotalMorale;
        public int LivingCrew;

#if DEBUG
        [Space]
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public bool RefreshNow;
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public bool GenerateCrew;
#endif

        public void Update()
        {

#if DEBUG
            if (GenerateCrew)
            {
                GenerateCrew = false;
                Crew = PersonGenerator.CreateGroup().People;
            }

            if (RefreshNow)
            {
                RefreshNow = false;
                Refresh();
            }
#endif
        }

        public void Refresh()
        {
            var total = 0.0;
            var count = 0;
            foreach (var person in Crew)
            {
                if (person == null || person.HealthActual <= 0)
                    continue; // Dead
                total += person.Morale;
                count++;
            }
            LivingCrew = count;
            TotalMorale = total / count;
        }
    }
}
