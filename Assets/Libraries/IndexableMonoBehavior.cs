﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace UnityEngine
{
    /// <summary>
    /// Created by Katelyn Gigante
    /// </summary>
    public abstract class IndexableMonoBehavior : MonoBehaviour
    {
        private Dictionary<string, PropertyInfo> Props = new Dictionary<string, PropertyInfo>();
        public object this[string pname]{
            get{
                var prop = GetProp(pname);
                return prop.GetValue(this, null);
            }
            set{
                var prop = GetProp(pname);
                prop.SetValue(this, value, null);
            }
        }

        private PropertyInfo GetProp(string pname)
        {
            if (!Props.ContainsKey(pname))
                Props[pname]= this.GetType().GetProperties().Single(n => n.Name == pname);
            return Props[pname];
        }
    }

    public abstract class IndexableMonoBehavior<T> : MonoBehaviour
    {
        private Dictionary<string, PropertyInfo> Props = new Dictionary<string, PropertyInfo>();
        public T this[string pname]
        {
            get
            {
                var prop = GetProp(pname);
                return (T)prop.GetValue(this, null);
            }
            set
            {
                var prop = GetProp(pname);
                prop.SetValue(this, value, null);
            }
        }

        private PropertyInfo GetProp(string pname)
        {
            if (!Props.ContainsKey(pname))
                Props[pname] = this.GetType().GetProperties().Single(n => n.Name == pname);
            return Props[pname];
        }
    }

    public abstract class IndexableClass<T>
    {
        private Dictionary<string, PropertyInfo> Props = new Dictionary<string, PropertyInfo>();
        private Dictionary<string, FieldInfo> Fields = new Dictionary<string, FieldInfo>();

        public T this[string pname]
        {
            get
            {
                var prop = GetProp(pname);
                if (prop == null)
                {
                    var field = GetField(pname);
                    return (T)field.GetValue(this);
                }
                return (T)prop.GetValue(this, null);
            }
            set
            {
                var prop = GetProp(pname);
                if (prop == null)
                {
                    var field = GetField(pname);
                    field.SetValue(this, value);
                    return;
                }
                prop.SetValue(this, value, null);
            }
        }

        private PropertyInfo GetProp(string pname)
        {
            if (!Props.ContainsKey(pname))
                Props[pname] = this.GetType().GetProperties().FirstOrDefault(n => n.Name == pname);
            return Props[pname];
        }

        private FieldInfo GetField(string pname)
        {
            if (!Fields.ContainsKey(pname))
                Fields[pname] = this.GetType().GetFields().FirstOrDefault(n => n.Name == pname);
            return Fields[pname];
        }
    }
}

