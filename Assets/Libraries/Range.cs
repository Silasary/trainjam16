﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Librarys
{
    class Range
    {
        public Range(int min, int max)
        {
            Min = min;
            Max = max;
        }

        public int Max { get; private set; }
        public int Min { get; private set; }

        internal int GetRandomPoint()
        {
            return UnityEngine.Random.Range(Min, Max);
        }
    }
}
